//
//  AppDelegate.swift
//  MyWeather
//
//  Created by Anh Le on 20/07/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.boostrapWidow()
        CacheManager.shared.configure()
        return true
    }
    
    /// Bootstrap a new window with root view controller to display.
    private func boostrapWidow() {
        let viewController = WeatherListingBuilder().build()
        let navigationController = UINavigationController(rootViewController: viewController)
        window = UIWindow()
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

