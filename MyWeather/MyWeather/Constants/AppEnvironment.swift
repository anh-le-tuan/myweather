//
//  AppEnvironment.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

struct AppEnvironment {
    
    static var apiEndpoint: String {
        "https://api.openweathermap.org/"
    }
    
    // Should not store it in source code like that, I will store it in my own server and get the value when I first time installed the app
    // Step 1: First time users install apps - get app_id from server
    // Step 2: Store the value in keychain
    // Step 3: For more secure, create a private key and store it in Secure Enclave, then encrypt app_id with public key (get from the private key) and store the cipherdata into keychain
    // Step 4: Everytime users open apps - get private key from Secure Enclave (use biometric if needed), then decrypt the ciperdata from keychain with the private key -> we can have app_id
    static var appId: String {
        "60c6fbeb4b93ac653c492ba806fc346d"
    }
}
