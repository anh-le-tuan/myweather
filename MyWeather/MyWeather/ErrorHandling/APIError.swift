//
//  APIError.swift
//  MyWeather
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation

enum APIError: Error {
    case invalidURL
    case serviceReponseError(String)
    case unexpectedResponse
    case decodeError
    case custom(String)
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case .serviceReponseError(let message): return message
        case .unexpectedResponse: return "Unexpected error response"
        case .decodeError: return "Decode Error"
        case .custom(let message): return message
        }
    }
}
