//
//  ErrorHandler.swift
//  MyWeather
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation

struct ErrorHandler {
    static let shared = ErrorHandler()
    
    private let genericMessage = "Something went wrong!"
    
    func handleError(_ error: Error, returnSpecific: Bool = true) -> String {
        guard returnSpecific else {
            return genericMessage
        }
        return error.localizedDescription
    }
}
