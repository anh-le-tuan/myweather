//
//  ValidationError.swift
//  MyWeather
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation

enum ValidationError: Error {
    case noEmptyValueAllowed(String)
}

extension ValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case let .noEmptyValueAllowed(target):
            return "The \(target) is empty"
        }
    }
}
