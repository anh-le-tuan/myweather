//
//  Array+Extension.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

extension Array {
    subscript(exist index: Index) -> Iterator.Element? {
        startIndex <= index && index < endIndex
        ? self[index]
        : nil
    }
}
