//
//  Date+Extension.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

extension Date {
    func toString(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.string(from: self)
    }
}
