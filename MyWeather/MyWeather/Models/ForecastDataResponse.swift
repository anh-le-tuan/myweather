//
//  ForecastDataResponse.swift
//  MyWeather
//
//  Created by Anh Le on 20/07/2022.
//

import Foundation

struct ForecastDataResponse: Codable {
    let city: City?
    let list: [ForecastDaily]?
    
    struct City: Codable {
        let id: Int?
        let name: String?
    }

    struct ForecastDaily: Codable {
        let dt: Int?
        let temp: Temp?
        let pressure, humidity: Int?
        let weather: [Weather]?
        
        var dateString: String? {
            guard let dt = dt else { return nil }
            let date = Date(timeIntervalSince1970: TimeInterval(dt))
            return date.toString(format: "E, d MMM yyyy")
        }

        enum CodingKeys: String, CodingKey {
            case dt, temp
            case pressure, humidity, weather
        }
        
        struct Temp: Codable {
            let min, max: Double?
            
            var avgString: String? {
                guard let min = min, let max = max else { return nil }
                return String(format: "%.0fºC", (min + max)/2)
            }
        }
        
        struct Weather: Codable {
            let id: Int?
            let main, weatherDescription, icon: String?

            enum CodingKeys: String, CodingKey {
                case id, main
                case weatherDescription = "description"
                case icon
            }
        }
    }
}
