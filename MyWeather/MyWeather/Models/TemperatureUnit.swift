//
//  TemperatureUnit.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

enum TemperatureUnit: String {
    // Celcius: C degree
    case metric
    // Fahrenheit: F degree
    case imperial
}
