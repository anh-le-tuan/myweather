//
//  APICall.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

protocol APICallProtocol {
    func call<Value: Decodable>(target: TargetType, promise: @escaping (Result<Value, Error>) -> Void)
}

final class APICall: NSObject, APICallProtocol {
    
    private var session: URLSession?
    
    override init() {
        super.init()
        self.session = DefaultURLSession.makeSession(urlCache: CacheManager.shared.cache, delegate: self)
    }
    
    convenience init(config: URLSessionConfiguration) {
        self.init()
        self.session = DefaultURLSession.makeSession(config: config, urlCache: CacheManager.shared.cache, delegate: self)
    }
    
    func call<Value: Decodable>(target: TargetType, promise: @escaping (Result<Value, Error>) -> Void) {
        guard let request = target.makeRequest() else { return }

        if let cachedDataResponse = CacheManager.shared.cachedDataResponse(for: request) {
            self.dataHandler(data: cachedDataResponse.data, promise: promise)
            return
        }

        let task = session?.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else {
                return promise(.failure(APIError.unexpectedResponse))
            }
            guard let response = response as? HTTPURLResponse,  // is there HTTP response
                  200 ..< 300 ~= response.statusCode
            else {
                return self.errorHandler(responseData: data, error: error, promise: promise)
            }
            guard let data = data else {
                return promise(.failure(APIError.unexpectedResponse))
            }

            CacheManager.shared.storeCached(response: response, data: data, for: request)
            self.dataHandler(data: data, promise: promise)
        }
        task?.resume()
    }
    
    private func dataHandler<Output: Decodable>(data: Data, promise: @escaping (Result<Output, Error>) -> Void) {
        do {
            let decodeValue = try JSONDecoder().decode(Output.self, from: data)
            promise(.success(decodeValue))
        } catch {
            promise(.failure(APIError.decodeError))
        }
    }
    
    private func errorHandler<Output>(responseData: Data?, error: Error?, promise: @escaping (Result<Output, Error>) -> Void) {
        guard let data = responseData,
              let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
              let message = json["message"] as? String else {
                  return promise(.failure(APIError.unexpectedResponse))
              }
        
        promise(.failure(APIError.serviceReponseError(message)))
    }
}

extension APICall: URLSessionDelegate, URLSessionDataDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let serverTrust = challenge.protectionSpace.serverTrust else {
            return completionHandler(.cancelAuthenticationChallenge, nil)
        }
        let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0)

        // Set SSL policies for domain name checking
        let policies = NSMutableArray()
        policies.add(SecPolicyCreateSSL(true, challenge.protectionSpace.host as NSString))
        SecTrustSetPolicies(serverTrust, policies)

        // Evaluate server certificate
        DispatchQueue.global().async {
            var error: CFError?

            // Since this function can block during those operations, try to call it in global queue
            let isServerTrusted: Bool = SecTrustEvaluateWithError(serverTrust, &error)

            // Get local and remote certificate data and compare them
            guard let serverCert = serverCertificate,
                  let localCert = CertificatesManager.openweathermap else {
                      return completionHandler(.cancelAuthenticationChallenge, nil)
                  }

            let server = SecCertificateCopyData(serverCert) as NSData
            let localCertData = SecCertificateCopyData(localCert) as Data

            var string: CFString?
            SecCertificateCopyCommonName(serverCert, &string)

            if (isServerTrusted && server.isEqual(to: localCertData)) {
                let credential = URLCredential(trust: serverTrust)
                completionHandler(.useCredential, credential)
            } else {
                return completionHandler(.cancelAuthenticationChallenge, nil)
            }
        }
    }
}
