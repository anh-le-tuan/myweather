//
//  DefaultURLSession.swift
//  MyWeather
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation

struct DefaultURLSession {
    static func makeSession(
        config: URLSessionConfiguration = URLSessionConfiguration.default,
        urlCache: URLCache?,
        delegate: URLSessionDelegate?
    ) -> URLSession {
        config.urlCache = urlCache
        return URLSession(configuration: config, delegate: delegate, delegateQueue: nil)
    }
}
