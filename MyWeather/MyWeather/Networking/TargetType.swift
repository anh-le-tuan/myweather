//
//  TargetType.swift
//  MyWeather
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation

enum HTTPMethod: String {
    case post = "POST"
    case get = "GET"
}

protocol TargetType {
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: [String: Any]? { get }
    var urlParams: [String: Any]? { get }
}

extension TargetType {
    var baseUrl: String {
        AppEnvironment.apiEndpoint + path
    }
    
    func makeRequest() -> URLRequest? {
        guard var urlComponents = URLComponents(string: baseUrl) else { return nil }
        
        // MARK: Temporary support query params only
        urlComponents.queryItems = urlParams?.map({ (key, value) in
                URLQueryItem(name: key, value: value as? String)
            })
        urlComponents.queryItems?.append(URLQueryItem(name: "appid", value: AppEnvironment.appId))
        urlComponents.queryItems?.sort(by: { $0.name > $1.name })
        
        guard let url = urlComponents.url else { return nil }
        
        var request = URLRequest(url: url, timeoutInterval: 30)
        request.httpMethod = method.rawValue
        return request
    }
}
