//
//  RemoteForecastRepository.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

protocol RemoteForecastRepository {
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    )
}

final class RemoteForecastRepositoryImpl: RemoteForecastRepository {
    private let service: ForecastService
    
    init(service: ForecastService) {
        self.service = service
    }
    
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    ) {
        self.service.getForecastListing(cityName: cityName, numberOfForcastDays: numberOfForcastDays, units: units, promise: promise)
    }
}
