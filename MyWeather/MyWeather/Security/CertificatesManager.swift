//
//  CertificatesManager.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

final class CertificatesManager {
    static let openweathermap = CertificatesManager.certificate(filename: "openweathermap.org")
    
    private static func certificate(filename: String) -> SecCertificate? {
        guard let filePath = Bundle.main.path(forResource: filename, ofType: "cer"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)),
              let certificate = SecCertificateCreateWithData(nil, data as CFData) else {
                  return nil
              }
        return certificate
    }
}
