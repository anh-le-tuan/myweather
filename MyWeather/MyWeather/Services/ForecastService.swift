//
//  ForecastService.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

protocol ForecastService {
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    )
}

final class ForecastServiceImpl: ForecastService {
    private let apiCall: APICallProtocol
    
    init(apiCall: APICallProtocol = APICall()) {
        self.apiCall = apiCall
    }
    
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    ) {
        apiCall.call(target: ForecastApiEndpoint.listing(cityName: cityName, numberOfForcastDays: numberOfForcastDays, units: units), promise: promise)
    }
}

enum ForecastApiEndpoint {
    case listing(cityName: String, numberOfForcastDays: Int?, units: TemperatureUnit?)
}

extension ForecastApiEndpoint: TargetType {
    var path: String {
        "data/2.5/forecast/daily"
    }
    
    var method: HTTPMethod {
        switch self {
        case .listing:
            return .get
        }
    }
    
    var headers: [String : Any]? {
        nil
    }
    
    var urlParams: [String : Any]? {
        switch self {
        case let .listing(name, cnt, units):
            let params: [String: Any] = ([
                "q": name,
                "cnt": cnt,
                "units": units?.rawValue
            ] as [String: Any?]).compactMapValues { $0 }
            return params
        }
    }
}
