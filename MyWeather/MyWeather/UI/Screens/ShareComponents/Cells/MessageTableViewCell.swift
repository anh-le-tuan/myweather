//
//  MessageTableViewCell.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
import UIKit

final class MessageTableViewCell: UITableViewCell, Reusable {
    
    // MARK: Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIs
    
    private lazy var messageLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.font = UIFont.preferredFont(forTextStyle: .body)
        view.adjustsFontForContentSizeCategory = true
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: Side effects
    
    private func setupViews() {
        self.contentView.addSubview(messageLabel)
        
        NSLayoutConstraint.activate([
            messageLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 15),
            messageLabel.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            messageLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            messageLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20),
        ])
    }
    
    func configure(message: String) {
        setupAccessibility(message: message)
        messageLabel.text = message
    }
    
    private func setupAccessibility(message: String) {
        self.isAccessibilityElement = true
        self.accessibilityValue = message
    }
}
