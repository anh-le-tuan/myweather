//
//  WeatherInfoTableViewCell.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
import UIKit
import Kingfisher

fileprivate enum LabelType: String, CaseIterable {
    case date = "Date: "
    case avgTemp = "Average Temperature: "
    case pressure = "Pressure: "
    case humidity = "Humidity: "
    case description = "Description: "
}

final class WeatherInfoTableViewCell: UITableViewCell, Reusable {
    
    // MARK: Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIs
    
    private(set) lazy var contentStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .leading
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private(set) lazy var iconImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: Side effects
    
    private func setupViews() {
        self.contentView.addSubview(contentStackView)
        self.contentView.addSubview(iconImageView)
        
        NSLayoutConstraint.activate([
            contentStackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10),
            contentStackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            contentStackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10),
            
            iconImageView.leadingAnchor.constraint(equalTo: self.contentStackView.trailingAnchor, constant: 10),
            iconImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            iconImageView.widthAnchor.constraint(equalToConstant: 70),
            iconImageView.heightAnchor.constraint(equalTo: iconImageView.widthAnchor),
            iconImageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10),
        ])
    }
    
    func configure(with data: ForecastDataResponse.ForecastDaily, cityName: String?) {
        for subView in contentStackView.arrangedSubviews {
            subView.removeFromSuperview()
        }
        for type in LabelType.allCases {
            switch type {
            case .date:
                contentStackView.addArrangedSubview(setupLabel(type: type, value: data.dateString))
            case .avgTemp:
                contentStackView.addArrangedSubview(setupLabel(type: type, value: data.temp?.avgString))
            case .pressure:
                contentStackView.addArrangedSubview(setupLabel(type: type, value: data.pressure.map(String.init)))
            case .humidity:
                contentStackView.addArrangedSubview(setupLabel(type: type, value: data.humidity.map {"\($0)%"}))
            case .description:
                contentStackView.addArrangedSubview(setupLabel(type: type, value: data.weather?.first?.weatherDescription))
            }
        }
        self.setupImage(with: data.weather?.first?.icon)
        
        self.setupAccessibility(with: data, cityName: cityName)
    }
    
    private func setupLabel(type: LabelType, value: String?) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.adjustsFontForContentSizeCategory = true
        label.isHidden = true
        guard let value = value else {
            return label
        }
        label.isHidden = false
        label.text = type.rawValue + value
        return label
    }
    
    private func setupImage(with icon: String?) {
        guard let icon = icon else {
            return
        }
        // temporary hard code here
        let url = URL(string: "\(icon)@2x.png", relativeTo: URL(string: "https://openweathermap.org/img/wn/"))
        iconImageView.kf.setImage(with: url)
    }
    
    private func setupAccessibility(with data: ForecastDataResponse.ForecastDaily, cityName: String?) {
        self.isAccessibilityElement = true
        
        self.accessibilityLabel = "On \(data.dateString ?? "Unknown day"), \(cityName ?? "Unknown city") has \(data.weather?.first?.weatherDescription ?? "Unknown weather") and average temperature is \(data.temp?.avgString ?? "Unknown temperature"), it's pressure is \(data.pressure.map(String.init) ?? "Unknown") and finally the humidity is \(data.humidity.map(String.init) ?? "Unknown")%"
        self.accessibilityValue = "Date is \(data.dateString ?? "Unknown day"), Average temperature is \(data.temp?.avgString ?? "Unknown temperature")"
    }
}
