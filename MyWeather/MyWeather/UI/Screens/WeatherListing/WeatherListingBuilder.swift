//
//  WeatherListingBuilder.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
import UIKit

final class WeatherListingBuilder {
    func build() -> UIViewController {
        let presenter = WeatherListingPresenter()
        let viewController = WeatherListingViewController(presenter: presenter)
        presenter.view = viewController
        return viewController
    }
}
