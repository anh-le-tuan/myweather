//
//  WeatherListingPresenter.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

protocol WeatherListingViewable: AnyObject {
    func toggleLoading(_ isEnabled: Bool)
    
    func reloadData()
}

struct WeatherListingSection {
    var items: [WeatherListingItem]
}

enum WeatherListingItem {
    case daily(String?, ForecastDataResponse.ForecastDaily)
    case error(String)
}

final class WeatherListingPresenter: WeatherListingPresentable {
    
    // MARK: Dependencies
    
    weak var view: WeatherListingViewable?
    
    private let forecastUseCase: ForecastUseCase
    
    // MARK: Misc
    
    var sections: [WeatherListingSection] = []
    
    // MARK: Init
    
    init(
        forecastUseCase: ForecastUseCase = ForecastUseCaseImpl(remoteRepository: RemoteForecastRepositoryImpl(service: ForecastServiceImpl()))
    ) {
        self.forecastUseCase = forecastUseCase
    }
    
    // MARK: Side Effects
    
    private func reloadData(searchText: String = "") {
        view?.toggleLoading(true)
        forecastUseCase.getForecastListing(
            cityName: searchText,
            numberOfForcastDays: 7,
            units: .metric
        ) { [weak self] (result: Result<ForecastDataResponse, Error>) in
            /*
             Actually, even if we use strong self here, it doesn't cause a memory leak.
             However, since this closure is escaping, then we don't know exactly when it will be deinit, maybe 30 seconds, 2 hours or even 100 years.
             -> This presenter will be deinit only when this closure deinit.
             To solve this problem, I use [weak self] to make the presenter deinit immediately and is not dependent on this escaping closure.
            */
            self?.view?.toggleLoading(false)
            self?.reloadDataHandler(result: result)
        }
    }
    
    private func reloadDataHandler(result: Result<ForecastDataResponse, Error>) {
        switch result {
        case let .success(forecastData):
            self.sections = self.makeSections(withData: forecastData, errorMessage: nil)
            self.view?.reloadData()
        case let .failure(error):
            self.sections = self.makeSections(withData: nil, errorMessage: ErrorHandler.shared.handleError(error, returnSpecific: false))
            self.view?.reloadData()
        }
    }
    
    private func makeSections(withData forecastData: ForecastDataResponse?, errorMessage: String?) -> [WeatherListingSection] {
        if let errorMessage = errorMessage {
            return [WeatherListingSection(items: [.error(errorMessage)])]
        }
        let forecastItems: [WeatherListingItem] = forecastData?.list?.map { .daily(forecastData?.city?.name, $0) } ?? []
        return [WeatherListingSection(items: forecastItems)]
    }
    
    // MARK: WeatherListingPresentable
    
    func viewDidLoad() {
        reloadData()
    }
    
    func numberOfSections() -> Int {
        sections.count
    }
    
    func numberOfRowsInSection(at index: Int) -> Int {
        sections[exist: index]?.items.count ?? 0
    }
    
    func item(at indexPath: IndexPath) -> WeatherListingItem? {
        sections[exist: indexPath.section]?.items[exist: indexPath.row]
    }
    
    func search(with searchText: String?) {
        guard let text = searchText, text.count >= 3 || text.isEmpty else { return }
        reloadData(searchText: text)
    }
}
