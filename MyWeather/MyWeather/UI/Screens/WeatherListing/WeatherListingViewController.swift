//
//  WeatherViewController.swift
//  MyWeather
//
//  Created by Anh Le on 20/07/2022.
//

import UIKit

protocol WeatherListingPresentable {
    func viewDidLoad()
    
    func numberOfSections() -> Int
    
    func numberOfRowsInSection(at index: Int) -> Int
    
    func item(at indexPath: IndexPath) -> WeatherListingItem?
    
    func search(with searchText: String?)
}

final class WeatherListingViewController: UIViewController, WeatherListingViewable, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: UIs

    private(set) lazy var searchController: UISearchController = {
        let view = UISearchController(searchResultsController: nil)
        view.searchResultsUpdater = self
        view.hidesNavigationBarDuringPresentation = false
        return view
    }()
    
    private(set) lazy var tableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(cellType: WeatherInfoTableViewCell.self)
        view.register(cellType: MessageTableViewCell.self)
        return view
    }()
    
    private(set) lazy var loadingIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.hidesWhenStopped = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func loadView() {
        super.loadView()
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.loadingIndicator)
        NSLayoutConstraint.activate([
            self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.tableView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.tableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            
            self.loadingIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.loadingIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
        ])
    }
    
    // MARK: Dependencies
    
    private let presenter: WeatherListingPresentable
    
    init(
        presenter: WeatherListingPresentable
    ) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecyles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white

        self.title = "Weather Forecast"
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        self.presenter.viewDidLoad()
    }
    
    // MARK: WeatherListingViewable
    
    func toggleLoading(_ isEnabled: Bool) {
        DispatchQueue.main.async {
            if isEnabled {
                self.loadingIndicator.startAnimating()
            } else {
                self.loadingIndicator.stopAnimating()
            }
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfRowsInSection(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = presenter.item(at: indexPath) else {
            return tableView.dequeueReusableCell(for: indexPath, cellType: MessageTableViewCell.self)
        }
        switch item {
        case let .daily(cityName, forecastData):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: WeatherInfoTableViewCell.self)
            cell.configure(with: forecastData, cityName: cityName)
            return cell
        case let .error(message):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: MessageTableViewCell.self)
            cell.configure(message: message)
            return cell
        }
    }
}

extension WeatherListingViewController: UISearchResultsUpdating {
    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        presenter.search(with: searchController.searchBar.text)
    }
}
