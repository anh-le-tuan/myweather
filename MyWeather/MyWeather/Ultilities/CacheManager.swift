//
//  CacheManager.swift
//  MyWeather
//
//  Created by Anh Le on 22/07/2022.
//

import Foundation

final class CacheManager {
    static let shared = CacheManager()
    
    private(set) var cachedExpiredTimeInterval: TimeInterval = 0
    
    private(set) var cache: URLCache?
    
    func configure(
        memoryCapacity: Int = 10*1024*1024, // Default is 10 MB
        diskCapacity: Int = 100*1024*1024, // Default is 100 MB
        expiredTime: TimeInterval = 60 * 5, // Default is 5 mins
        directoryName: String = "WeatherCached"
    ) {
        let cachesURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let diskCacheURL = cachesURL.appendingPathComponent(directoryName)
        cache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, directory: diskCacheURL)
        cachedExpiredTimeInterval = expiredTime
    }
    
    func storeCached(response: URLResponse, data: Data, for request: URLRequest) {
        let cachedResponse = CachedURLResponse(response: response, data: data, userInfo: ["expiredDate": Date().addingTimeInterval(cachedExpiredTimeInterval)], storagePolicy: .allowed)
        cache?.storeCachedResponse(cachedResponse, for: request)
    }
    
    func cachedDataResponse(for request: URLRequest) -> CachedURLResponse? {
        guard let cachedResponse = cache?.cachedResponse(for: request),
              let expiredDate = cachedResponse.userInfo?["expiredDate"] as? Date,
              Date() < expiredDate else {
                  cache?.removeCachedResponse(for: request)
                  return nil
              }
        return cachedResponse
    }
}
