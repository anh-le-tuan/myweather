//
//  Reusable.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

// MARK: Protocol definition

/// Make your `UITableViewCell` and `UICollectionViewCell` subclasses
/// conform to this protocol when they are *not* NIB-based but only code-based
/// to be able to dequeue them in a type-safe manner
public protocol Reusable: AnyObject {
    static var reuseIdentifier: String { get }
}

// MARK: - Default implementation

public extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
