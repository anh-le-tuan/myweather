//
//  ForecastUseCase.swift
//  MyWeather
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation

protocol ForecastUseCase {
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    )
}

final class ForecastUseCaseImpl: ForecastUseCase {
    
    private let remoteRepository: RemoteForecastRepository
    
    init(
        remoteRepository: RemoteForecastRepository
    ) {
        self.remoteRepository = remoteRepository
    }
    
    func getForecastListing(
        cityName: String,
        numberOfForcastDays: Int?,
        units: TemperatureUnit?,
        promise: @escaping (Result<ForecastDataResponse, Error>) -> Void
    ) {
        self.remoteRepository.getForecastListing(cityName: cityName, numberOfForcastDays: numberOfForcastDays, units: units, promise: promise)
    }
}
