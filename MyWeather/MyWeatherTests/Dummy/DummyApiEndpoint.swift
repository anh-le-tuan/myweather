//
//  DummyApiEndpoint.swift
//  MyWeatherTests
//
//  Created by Anh Le on 25/07/2022.
//

import Foundation
@testable import MyWeather

enum DummyApiEndpoint: TargetType {
    case dummyData
    
    var path: String {
        return ""
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: [String : Any]? {
        return nil
    }
    
    var urlParams: [String : Any]? {
        return nil
    }
}
