//
//  ForecastDataTests.swift
//  MyWeatherTests
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation
import Quick
import Nimble

@testable import MyWeather

final class ForecastDataTests: QuickSpec {
    override func spec() {
        var sut: ForecastDataResponse!
        describe("The 'ForecastDataResponse'") {
            
            beforeEach {
                sut = ForecastDataResponse(city: nil, list: [
                    ForecastDataResponse.ForecastDaily(dt: 1658677440, temp: ForecastDataResponse.ForecastDaily.Temp(min: 20, max: 30), pressure: nil, humidity: nil, weather: nil)
                ])
            }
            
            it("should convert to correct string date") {
                expect(sut.list?.first?.dateString).to(equal("Sun, 24 Jul 2022"))
                sut = ForecastDataResponse(city: nil, list: [
                    ForecastDataResponse.ForecastDaily(dt: 1628677440, temp: nil, pressure: nil, humidity: nil, weather: nil)
                ])
                expect(sut.list?.first?.dateString).to(equal("Wed, 11 Aug 2021"))
            }
            
            it("should calculate correct avg temp") {
                expect(sut.list?.first?.temp?.avgString).to(equal("25ºC"))
                sut = ForecastDataResponse(city: nil, list: [
                    ForecastDataResponse.ForecastDaily(dt: nil, temp: ForecastDataResponse.ForecastDaily.Temp(min: 20.5, max: 28.3), pressure: nil, humidity: nil, weather: nil)
                ])
                expect(sut.list?.first?.temp?.avgString).to(equal("24ºC"))
            }
        }
    }
}
