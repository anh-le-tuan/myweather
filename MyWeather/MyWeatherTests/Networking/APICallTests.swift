//
//  APICallTests.swift
//  MyWeatherTests
//
//  Created by Anh Le on 25/07/2022.
//

import Foundation
import Quick
import Nimble

@testable import MyWeather

final class APICallTests: QuickSpec {
    override func spec() {
        let testConfig : URLSessionConfiguration = {
            let config = URLSessionConfiguration.ephemeral
            config.protocolClasses = [MockURLProtocol.self]
            return config
        }()
        let sut: APICall = APICall(config: testConfig)
        describe("The 'APICall'") {
            
            beforeEach {
                // Clear cache before run every test cases
                CacheManager.shared.cache?.removeAllCachedResponses()
                self.resetRequestHandler()
            }
            
            context("Test function call When API return data") {
                it("should return data when status code is 200") {
                    let forecastData = ForecastDataResponse(city: ForecastDataResponse.City(id: 1111, name: "sai gon"), list: nil)
                    let data = try! JSONEncoder().encode(forecastData)
                    self.setRequestHandler(with: data, error: nil, statusCode: 200)
                    waitUntil(timeout: DispatchTimeInterval.seconds(2)) { done in
                        sut.call(target: DummyApiEndpoint.dummyData) { (result: Result<ForecastDataResponse, Error>) in
                            expect((try? result.get().city?.id)).to(equal(1111))
                            expect((try? result.get().city?.name)).to(equal("sai gon"))
                            done()
                        }
                    }
                }
                it("should return error unexpected response when status code is 400 and data didn't return message") {
                    let forecastData = ForecastDataResponse(city: ForecastDataResponse.City(id: 1111, name: "sai gon"), list: nil)
                    let data = try! JSONEncoder().encode(forecastData)
                    self.setRequestHandler(with: data, error: nil, statusCode: 400)
                    waitUntil(timeout: DispatchTimeInterval.seconds(2)) { done in
                        sut.call(target: DummyApiEndpoint.dummyData) { (result: Result<ForecastDataResponse, Error>) in
                            switch result {
                            case .failure(let error):
                                let errorMessage = ErrorHandler.shared.handleError(error)
                                expect(errorMessage).to(equal("Unexpected error response"))
                            default:
                                fail()
                            }
                            done()
                        }
                    }
                }
            }
            
            context("Test function call When API did not return data") {
                it("should return error Decode Error when status code is 200") {
                    // Mock cannot return data nil, it returns data with 0 byte
                    self.setRequestHandler(with: nil, error: nil, statusCode: 200)
                    waitUntil(timeout: DispatchTimeInterval.seconds(2)) { done in
                        sut.call(target: DummyApiEndpoint.dummyData) { (result: Result<ForecastDataResponse, Error>) in
                            switch result {
                            case .failure(let error):
                                let errorMessage = ErrorHandler.shared.handleError(error)
                                expect(errorMessage).to(equal("Decode Error"))
                            default:
                                fail()
                            }
                            done()
                        }
                    }
                }
                it("should return error unexpected response when status code is 200") {
                    // Mock cannot return data nil, it returns data with 0 byte
                    self.setRequestHandler(with: nil, error: nil, statusCode: 200)
                    waitUntil(timeout: DispatchTimeInterval.seconds(2)) { done in
                        sut.call(target: DummyApiEndpoint.dummyData) { (result: Result<ForecastDataResponse, Error>) in
                            switch result {
                            case .failure(let error):
                                let errorMessage = ErrorHandler.shared.handleError(error)
                                expect(errorMessage).to(equal("Decode Error"))
                            default:
                                fail()
                            }
                            done()
                        }
                    }
                }
            }
        }
    }
    
    private func setRequestHandler(with data: Data?, error: Error?, statusCode: Int) {
        MockURLProtocol.requestHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
            return (response, data, error)
        }
    }
    
    private func resetRequestHandler() {
        MockURLProtocol.requestHandler = nil
    }
}
