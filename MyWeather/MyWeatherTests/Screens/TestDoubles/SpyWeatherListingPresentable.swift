//
//  SpyWeatherListingPresentable.swift
//  MyWeatherTests
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
@testable import MyWeather

final class SpyWeatherListingPresentable: WeatherListingPresentable {
    
    var invokedViewDidLoad = false
    var invokedViewDidLoadCount = 0
    
    func viewDidLoad() {
        invokedViewDidLoad = true
        invokedViewDidLoadCount += 1
    }
    
    var invokedNumberOfSections = false
    var invokedNumberOfSectionsCount = 0
    var stubbedNumberOfSectionsResult: Int! = 0
    
    func numberOfSections() -> Int {
        invokedNumberOfSections = true
        invokedNumberOfSectionsCount += 1
        return stubbedNumberOfSectionsResult
    }
    
    var invokedNumberOfRowsInSection = false
    var invokedNumberOfRowsInSectionCount = 0
    var stubbedNumberOfRowsInSectionResult: Int! = 0
    
    func numberOfRowsInSection(at index: Int) -> Int {
        invokedNumberOfRowsInSection = true
        invokedNumberOfRowsInSectionCount += 1
        return stubbedNumberOfRowsInSectionResult
    }
    
    var invokedItemAtIndexPath = false
    var invokedItemAtIndexPathCount = 0
    var stubbedItemAtIndexPathResult: WeatherListingItem?
    
    func item(at indexPath: IndexPath) -> WeatherListingItem? {
        invokedItemAtIndexPath = true
        invokedItemAtIndexPathCount += 1
        return stubbedItemAtIndexPathResult
    }
    
    var invokedSearchWithSearchText = false
    var invokedSearchWithSearchTextCount = 0
    
    func search(with searchText: String?) {
        invokedSearchWithSearchText = true
        invokedSearchWithSearchTextCount += 1
    }
}
