//
//  SpyWeatherListingViewable.swift
//  MyWeatherTests
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
@testable import MyWeather
import UIKit

final class SpyWeatherListingViewable: WeatherListingViewable {
    
    var invokedToogleLoading = false
    var invokedToogleLoadingCount = 0
    
    func toggleLoading(_ isEnabled: Bool) {
        invokedToogleLoading = true
        invokedToogleLoadingCount += 1
    }
    
    var invokedReloadData = false
    var invokedReloadDataCount = 0
    
    func reloadData() {
        invokedReloadData = true
        invokedReloadDataCount += 1
    }
}
