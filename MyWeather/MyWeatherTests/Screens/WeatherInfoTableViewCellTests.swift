//
//  WeatherInfoTableViewCellTests.swift
//  MyWeatherTests
//
//  Created by Anh Le on 25/07/2022.
//

import Foundation
import Quick
import Nimble
@testable import MyWeather

final class WeatherInfoTableViewCellTests: QuickSpec {
    override func spec() {
        var sut: WeatherInfoTableViewCell!
        
        describe("The 'WeatherInfoTableViewCell'") {
            beforeEach {
                sut = WeatherInfoTableViewCell()
            }
            context("Test init") {
                it("should add subviews") {
                    expect(sut.contentStackView.isDescendant(of: sut.contentView)).to(equal(true))
                    expect(sut.iconImageView.isDescendant(of: sut.contentView)).to(equal(true))
                }
            }
            context("Test configure function") {
                it("should add 5 labels") {
                    expect(sut.contentStackView.arrangedSubviews.count).to(equal(0))
                    
                    let forecastDaily = ForecastDataResponse.ForecastDaily(dt: 1658677440, temp: ForecastDataResponse.ForecastDaily.Temp(min: 20, max: 30), pressure: 1000, humidity: 1000, weather: [ForecastDataResponse.ForecastDaily.Weather(id: 12, main: nil, weatherDescription: "sunny", icon: "10d")])
                    sut.configure(with: forecastDaily, cityName: "sai gon")
                    sleep(2)
                    
                    expect(sut.contentStackView.arrangedSubviews.count).to(equal(5))
                }
            }
        }
    }
}
