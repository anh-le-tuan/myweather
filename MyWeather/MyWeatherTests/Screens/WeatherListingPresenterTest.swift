//
//  WeatherListingPresenterTest.swift
//  MyWeatherTests
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
import Quick
import Nimble
@testable import MyWeather

final class WeatherListingPresenterTest: QuickSpec {
    
    override func spec() {
        let stubRemoteForecastRepo = StubRemoteForecastRepository()
        let forecastUseCase = ForecastUseCaseImpl(remoteRepository: stubRemoteForecastRepo)
        var view: SpyWeatherListingViewable!
        let sut = WeatherListingPresenter(forecastUseCase: forecastUseCase)
        describe("The 'WeatherListingPresenter'") {
            
            beforeEach {
                sut.sections = []
                stubRemoteForecastRepo.stubResponse = .failure(APIError.unexpectedResponse)
                view = SpyWeatherListingViewable()
                sut.view = view
            }
            
            context("When viewDidLoad is called") {
                
                it("should have 1 section with a item Unexpected error") {
                    expect(sut.numberOfSections()).to(equal(0))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(0))
                    sut.viewDidLoad()
                    expect(sut.numberOfSections()).to(equal(1))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(1))
                    if case let WeatherListingItem.error(message) = sut.item(at: IndexPath(row: 0, section: 0))! {
                        expect(message).to(equal("Something went wrong!"))
                    } else {
                        fail("cast value wrong")
                    }
                }
                
                it("should have 1 section with a item has humidity 1000") {
                    let forecastDay = ForecastDataResponse.ForecastDaily(dt: nil, temp: nil, pressure: nil, humidity: 1000, weather: nil)
                    stubRemoteForecastRepo.stubResponse = .success(ForecastDataResponse(city: nil, list: [forecastDay]))
                    
                    expect(sut.numberOfSections()).to(equal(0))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(0))
                    sut.viewDidLoad()
                    expect(sut.numberOfSections()).to(equal(1))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(1))
                    if case let WeatherListingItem.daily(_, daily) = sut.item(at: IndexPath(row: 0, section: 0))! {
                        expect(daily.humidity).to(equal(1000))
                    } else {
                        fail("cast value wrong")
                    }
                }
                
                it("should have 1 section with none items") {
                    stubRemoteForecastRepo.stubResponse = .success(ForecastDataResponse(city: nil, list: nil))
                    
                    expect(sut.numberOfSections()).to(equal(0))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(0))
                    sut.viewDidLoad()
                    expect(sut.numberOfSections()).to(equal(1))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(0))
                }
                
                it("should trigger view reload data") {
                    expect(view.invokedReloadData).to(equal(false))
                    expect(view.invokedReloadDataCount).to(equal(0))
                    sut.viewDidLoad()
                    expect(view.invokedReloadData).to(equal(true))
                    expect(view.invokedReloadDataCount).to(equal(1))
                }
                
                it("should trigger view reload data") {
                    expect(view.invokedToogleLoading).to(equal(false))
                    expect(view.invokedToogleLoadingCount).to(equal(0))
                    sut.viewDidLoad()
                    expect(view.invokedToogleLoading).to(equal(true))
                    expect(view.invokedToogleLoadingCount).to(equal(2))
                }
                
            }
            
            context("When search is called") {
                it("should have 1 section with a item Unexpected error") {
                    sut.search(with: "test")
                    expect(sut.numberOfSections()).to(equal(1))
                    expect(sut.numberOfRowsInSection(at: 0)).to(equal(1))
                    if case let WeatherListingItem.error(message) = sut.item(at: IndexPath(row: 0, section: 0))! {
                        expect(message).to(equal("Something went wrong!"))
                    } else {
                        fail("cast value wrong")
                    }
                }
            }
            
        }
        
    }
    
}
