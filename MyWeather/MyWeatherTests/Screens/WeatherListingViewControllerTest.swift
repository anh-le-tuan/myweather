//
//  WeatherListingViewControllerTest.swift
//  MyWeatherTests
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
import Quick
import Nimble
@testable import MyWeather

final class WeatherListingViewControllerTest: QuickSpec {
    
    override func spec() {
        var presenter: SpyWeatherListingPresentable!
        var sut: WeatherListingViewController!
        describe("The 'WeatherListingViewController'") {
            
            beforeEach {
                presenter = SpyWeatherListingPresentable()
                sut = WeatherListingViewController(presenter: presenter)
                sut.loadViewIfNeeded()
            }
            
            context("When loadview is called") {
                it("should table view is descendant of view") {
                    expect(sut.tableView.isDescendant(of: sut.view)).to(equal(true))
                    expect(sut.loadingIndicator.isDescendant(of: sut.view)).to(equal(true))
                }
            }
            
            context("When viewDidLoad is called") {
                it("should invoke viewDidLoad of presenter") {
                    expect(presenter.invokedViewDidLoad).to(equal(true))
                }
            }
            
            context("When toogle loading is called") {
                it("should show/hide loading indicator") {
                    expect(sut.loadingIndicator.isHidden).to(equal(true))
                    
                    sut.toggleLoading(true)
                    
                    expect(sut.loadingIndicator.isHidden).toEventually(equal(false), timeout: DispatchTimeInterval.seconds(1))
                    
                    sut.toggleLoading(false)
                    
                    expect(sut.loadingIndicator.isHidden).toEventually(equal(true), timeout: DispatchTimeInterval.seconds(1))
                }
            }
            
            context("When numberOfSections is called") {
                it("should invoke presenter numberOfSections") {
                    expect(presenter.invokedNumberOfSections).to(equal(false))
                    expect(presenter.invokedNumberOfSectionsCount).to(equal(0))
                    
                    presenter.stubbedNumberOfSectionsResult = 3
                    expect(sut.numberOfSections(in: sut.tableView)).to(equal(presenter.stubbedNumberOfSectionsResult))
                    
                    expect(presenter.invokedNumberOfSections).to(equal(true))
                    expect(presenter.invokedNumberOfSectionsCount).to(equal(1))
                }
            }
            
            context("When numberOfRowsInSection is called") {
                it("should invoke presenter numberOfItemsInSection") {
                    expect(presenter.invokedNumberOfRowsInSection).to(equal(false))
                    expect(presenter.invokedNumberOfRowsInSectionCount).to(equal(0))
                    
                    presenter.stubbedNumberOfRowsInSectionResult = 3
                    expect(sut.tableView(sut.tableView, numberOfRowsInSection: 0)).to(equal(presenter.stubbedNumberOfRowsInSectionResult))
                    
                    expect(presenter.invokedNumberOfRowsInSection).to(equal(true))
                    expect(presenter.invokedNumberOfRowsInSectionCount).to(equal(1))
                }
            }
            
            context("When user text on search bar") {
                it("should call presenter") {
                    expect(presenter.invokedSearchWithSearchText).to(equal(false))
                    expect(presenter.invokedSearchWithSearchTextCount).to(equal(0))
                    
                    sut.searchController.searchBar.text = "sa"
                    
                    expect(presenter.invokedSearchWithSearchText).to(equal(true))
                    expect(presenter.invokedSearchWithSearchTextCount).to(equal(1))
                }
            }
            
            context("When cellForRow is called") {
                it("should return MessageTableViewCell when presenter pass data is nil") {
                    expect(presenter.invokedItemAtIndexPath).to(equal(false))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(0))
                    
                    presenter.stubbedItemAtIndexPathResult = nil
                    expect(sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) is MessageTableViewCell).to(equal(true))
                    
                    expect(presenter.invokedItemAtIndexPath).to(equal(true))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(1))
                }
                
                it("should return WeatherInfoTableViewCell when presenter pass data is daily forecast") {
                    expect(presenter.invokedItemAtIndexPath).to(equal(false))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(0))
                    
                    presenter.stubbedItemAtIndexPathResult = WeatherListingItem.daily(nil, ForecastDataResponse.ForecastDaily(dt: nil, temp: nil, pressure: nil, humidity: 1000, weather: nil))
                    expect(sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) is WeatherInfoTableViewCell).to(equal(true))
                    
                    expect(presenter.invokedItemAtIndexPath).to(equal(true))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(1))
                }
                
                it("should return MessageTableViewCell when presenter pass data is error") {
                    expect(presenter.invokedItemAtIndexPath).to(equal(false))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(0))
                    
                    presenter.stubbedItemAtIndexPathResult = WeatherListingItem.error("error")
                    expect(sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) is MessageTableViewCell).to(equal(true))
                    
                    expect(presenter.invokedItemAtIndexPath).to(equal(true))
                    expect(presenter.invokedItemAtIndexPathCount).to(equal(1))
                }
            }
        }
    }
}
