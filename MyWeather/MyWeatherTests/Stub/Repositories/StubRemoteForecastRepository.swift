//
//  StubRemoteForecastRepository.swift
//  MyWeatherTests
//
//  Created by Anh Le on 21/07/2022.
//

import Foundation
@testable import MyWeather

final class StubRemoteForecastRepository: RemoteForecastRepository {
    
    var stubResponse: Result<ForecastDataResponse, Error> = .failure(APIError.unexpectedResponse)
    
    func getForecastListing(cityName: String, numberOfForcastDays: Int?, units: TemperatureUnit?, promise: @escaping (Result<ForecastDataResponse, Error>) -> Void) {
        promise(stubResponse)
    }
}
