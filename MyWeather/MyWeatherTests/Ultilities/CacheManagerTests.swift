//
//  CacheManagerTests.swift
//  MyWeatherTests
//
//  Created by Anh Le on 25/07/2022.
//

import Foundation
import Quick
import Nimble

@testable import MyWeather

final class CacheManagerTests: QuickSpec {
    override func spec() {
        let sut: CacheManager = CacheManager.shared
        let directoryName: String = "test"
        let urlRequest = URLRequest(url: URL(string: "https://unittest")!)
        describe("The 'CacheManager'") {
            
            beforeEach {
                sut.configure(memoryCapacity: 1*1024*1024, diskCapacity: 1*1024*1024, expiredTime: 60, directoryName: directoryName)
            }
            
            context("Test configuration") {
                it("should return correct instance") {
                    sut.configure(memoryCapacity: 1*1024*1024, diskCapacity: 1*1024*1024, expiredTime: 60, directoryName: directoryName)
                    expect(sut.cache?.memoryCapacity).to(equal(1*1024*1024))
                    expect(sut.cache?.diskCapacity).to(equal(1*1024*1024))
                    expect(sut.cachedExpiredTimeInterval).to(equal(60))
                }
            }
            
            context("Test store cache") {
                it("should return data after store cache") {
                    expect(sut.cachedDataResponse(for: urlRequest)).to(beNil())
                    sut.storeCached(response: URLResponse(), data: Data(), for: urlRequest)
                    sleep(1)
                    expect(sut.cachedDataResponse(for: urlRequest)).toNot(beNil())
                }
            }
            
            context("Test get cache") {
                it("should not return data after time expired") {
                    sut.configure(memoryCapacity: 1*1024*1024, diskCapacity: 1*1024*1024, expiredTime: 3, directoryName: directoryName)
                    expect(sut.cachedDataResponse(for: urlRequest)).to(beNil())
                    sut.storeCached(response: URLResponse(), data: Data(), for: urlRequest)
                    sleep(1)
                    expect(sut.cachedDataResponse(for: urlRequest)).toNot(beNil())
                    sleep(3)
                    expect(sut.cachedDataResponse(for: urlRequest)).to(beNil())
                }
            }
            
            afterEach {
                sut.cache?.removeAllCachedResponses()
            }
        }
    }
}
