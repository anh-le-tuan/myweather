//
//  Common.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation
import XCTest

class Common: XCTestCase {
    lazy var app: XCUIApplication = {
        XCUIApplication()
    }()
    
    func launchApplication() {
        switch app.state {
        case .runningForeground:
            app.launch()
        case .runningBackground:
            app.activate()
        default:
            app.launch()
        }
    }
}
