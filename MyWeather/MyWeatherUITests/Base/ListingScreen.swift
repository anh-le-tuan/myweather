//
//  ListingScreen.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

import XCTest

class ListingScreen: Common {
    func typeOnSearchBar(text: String) {
        let searchBarElement = app.searchFields.firstMatch
        searchBarElement.tap()
        searchBarElement.typeText(text)
    }
    
    func checkLabelText(text: String) {
        sleep(2)
        let cells = app.tables.cells
        guard let valueString = cells.firstMatch.value as? String else {
            XCTFail("Cannot find accessibility value")
            return
        }
        XCTAssert(valueString.contains(text), "Cannot find results")
    }
    
    func typeOnSearchBarThenClearText(text: String) {
        let searchBarElement = app.searchFields.firstMatch
        searchBarElement.tap()
        searchBarElement.typeText(text)
        sleep(2)
        app.otherElements.staticTexts["Cancel"].getCoordinate().tap()
    }
    
    func checkMessageEmptySearchBar() {
        sleep(2)
        let cells = app.tables.cells
        guard let valueString = cells.firstMatch.value as? String else {
            XCTFail("Cannot find accessibility value")
            return
        }
        XCTAssert(valueString.contains("Something went wrong!"), "Cannot find results")
    }
}
