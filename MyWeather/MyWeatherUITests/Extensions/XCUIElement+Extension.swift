//
//  XCUIElement+Extension.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

import XCTest

extension XCUIElement {
    
    func getCoordinate() -> XCUICoordinate {
        return self.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0)).withOffset(CGVector(dx: 10, dy: 10))
    }
}
