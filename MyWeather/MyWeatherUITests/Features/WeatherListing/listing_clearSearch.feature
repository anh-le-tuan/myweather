Feature: Weather Listing: Search
  User can see the error message after clear search text

  @listing
  Scenario Outline: User can see validation error message after clear search text
    """
    Pass / Fail Condition
    If User clear search text and sees the validation error message, test is passed
    If not, test is failed
    """
    Given User open the application
    When User type a valid text that is <text> and clear text on search bar
    Then User can see validation error message
    
    Examples:
      | text        |
      | sai         |
