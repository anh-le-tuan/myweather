Feature: Weather Listing: Search
  User can see list forecast after search

  @listing
  Scenario Outline: User can see listing or error after search
    """
    Pass / Fail Condition
    If User searches with more than 3 characters on search bar, and sees data is shown, test is passed
    If not, test is failed
    """
    Given User open the application
    When User type <text> on search bar
    Then User can see data on the result area, with label contains <responseText>
    
    Examples:
      | text        | responseText              |
      | sai         | Average temperature is    |
      | gsdfkl      | Something went wrong!     |
