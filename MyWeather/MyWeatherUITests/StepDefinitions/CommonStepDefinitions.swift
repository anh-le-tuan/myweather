//
//  CommonStepDefinitions.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation
import Cucumberish

final class CommonStepDefinition: Common {

    func implementation() {
        Given("User open the application") { _, _ in
            self.launchApplication()
        }
    }
}
