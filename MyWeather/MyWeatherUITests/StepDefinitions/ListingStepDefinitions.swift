//
//  ListingStepDefinitions.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

import Foundation
import Cucumberish

final class ListingStepDefinitions: ListingScreen {
    
    func implementation() {
        When("User type ([^\\\"]*) on search bar") { args, _ in
            let searchText = args?.first ?? ""
            self.typeOnSearchBar(text: searchText)
        }
        
        Then("User can see data on the result area, with label contains ([^\\\"]*)") { args, _ in
            let labelText = args?.first ?? ""
            self.checkLabelText(text: labelText)
        }
        
        When("User type a valid text that is ([^\\\"]*) and clear text on search bar") { args, _ in
            let searchText = args?.first ?? ""
            self.typeOnSearchBarThenClearText(text: searchText)
        }
        
        Then("User can see validation error message") { _, _ in
            self.checkMessageEmptySearchBar()
        }
    }
}
