//
//  MyWeatherUITests.swift
//  MyWeatherUITests
//
//  Created by Anh Le on 20/07/2022.
//

import Foundation
import Cucumberish

class CucumberishInitializer: NSObject {
    @objc class func setupCucumberish()
    {
        setupStepDefinitions()
        
        let bundle = Bundle(for: CucumberishInitializer.self)
        Cucumberish.executeFeatures(inDirectory: "Features", from: bundle, includeTags: nil, excludeTags: nil)
    }
    
    class func setupStepDefinitions() {
        CommonStepDefinition().implementation()
        
        ListingStepDefinitions().implementation()
    }
}
