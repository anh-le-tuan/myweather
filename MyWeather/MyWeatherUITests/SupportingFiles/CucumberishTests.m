//
//  CucumberishTests.m
//  MyWeatherUITests
//
//  Created by Anh Le on 24/07/2022.
//

#import <Foundation/Foundation.h>
#import "MyWeatherUITests-Swift.h"

__attribute__((constructor))
void CucumberishInit( void )
{
    [CucumberishInitializer setupCucumberish];
}
