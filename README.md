# MyWeather

Welcome you to view my project, that serve comsumers view weather forcast. I'm using api from openweathermap.org and develop the iOS application by Swift.

## Getting started

Easy to run, please follow the below steps:
- Install Xcode 13
- Install CocoaPods
- Clone the project
- Go to `MyWeather` directory
- Run `pod install`
- Open file .xcworkspace
- Run and enjoy

## Architecture and design patterns

I'm using MVP for this project

### MVP

- `View` - delegates user interaction events to the `Presenter` and displays data passed by the `Presenter`
- `Presenter` - contains the presentation logic and tells the `View` what to present
- `Model` - normally the group of classes/types/components that represent the core domain (business or otherwise) that the application operates within

### Design patterns

- `Builder`
- `Singleton`
- `Facade`

## Code folder structure

- `App` - contains AppDelegate file
- `Resources` - contains resources files such as images, colors set,...
- `UI` - contains presentation files such as CustomView, CustomCell, ViewController, Presenter,...
- `Models` - contains models files that present data using in the application
- `Extension` - contains extensions files that make developers easily reuse functions and components
- `Ultilities` - contains util files that handle some specific jobs
- `Constant` - contains constant files that store variables using in the application
- `Security` - contains a certificate manager that can be used to manage certificates in the application to do SSL pinning
- `UseCases` - contains use case files that handle business logic
- `Repositories` - contains repository files used for wrap service calls such as remote repository can use remote service to call API, and local repository can contact local service to obtain data from persistent storage
- `Services` - contains services files used for connecting with external sources such as API or files

## Third parties frameworks

- `Kingfisher` - I'm using it for loading and caching images and then presenting them onto the screen
- `Quick` - is a behavior-driven development framework for Swift and Objective-C
- `Nimble` - express the expected outcomes of Swift or Objective-C expressions

## Checklist of items that has been DONE

- [x] The application is a simple iOS application which is written by Swift.
- [x] The application is able to retrieve the weather information from OpenWeatherMaps API.
- [x] The application is able to allow user to input the searching term.
- [x] The application is able to proceed searching with a condition of the search term length must be from 3 characters or above.
- [x] The application is able to render the searched results as a list of weather items.
- [x] The application is able to support caching mechanism so as to prevent the app from generating a bunch of API requests.
- [x] The application is able to manage caching mechanism & lifecycle.
- [x] The application is able to load the weather icons remotely and displayed on every weather item at the right-hand-side.
- [x] The application is able to handle failures.
- [x] The application is able to support the disability to scale large text for who can't see the text clearly.
- [x] The application is able to support the disability to read out the text using VoiceOver controls.
- [x] Apply SSL Pinning to protect the application when it transfers data between client and server
